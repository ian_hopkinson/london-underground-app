FROM ubuntu:15.04

RUN apt-get update && \
    apt-get install -y \
        curl \
        git \
        python-pip \
        gunicorn

RUN mkdir /home/london-underground && \
    chown nobody /home/london-underground

USER nobody
ENV HOME=/home/london-underground
WORKDIR /home/london-underground

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000"]
EXPOSE 8000
CMD ["--log-file", "-", "--access-logfile", "-", "app:app"]

COPY ./requirements.txt /home/london-underground/app/
RUN pip install --user -r /home/london-underground/app/requirements.txt
COPY ./app /home/london-underground/app/