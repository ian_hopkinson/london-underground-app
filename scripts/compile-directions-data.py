#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals


import codecs
import csv
import json
import math
import os
import sys

import requests
# import requests_cache

from collections import OrderedDict

# requests_cache.install_cache('google_directions_api_cache')

stations = {}
walk_threshold = 200.0

# Data for crowflies vs Google directions comparisons for Charing Cross origin are in:
# charing-cross-data-2015-09-01.xlsx
# 
# Mapbox directions API looks interesting - designed for exactly this problem:
# https://www.mapbox.com/developers/api/distance/?utm_source=directionsupdate&utm_medium=email&utm_content=getstarteddocs&utm_campaign=distancelaunch
# TODO


METRES_PER_MILE = 1609
def main():
    global stations
    lookup = []
# Read in original locations file
    stations = read_stations_file("../app/static/station_data.csv")
# Read in dictionary file, if available
    
# Select a starting station
    origin = "Chesham"
# Loop over all stations
    for row in stations:
        journey = get_station_data(origin, row['station'])
        lookup.append(journey)

# Print results
    print("\nStarting from {}\n".format(origin))
    print("{:25s} {:>12s} {:>12s} {:>12s} {:>12s} {:>12s}".format("station", "crowflies", "api_distance", "distance_ratio", "api_duration", "api_speed"))
    for row in lookup:
        if row['api_distance'] is not None:
            print("{:25s} {:12.2f} {:12.2f} {:12.2f} {:12.2f} {:12.2f}".format(row['finish'].replace(" ", "_"), 
                                                       row['crowflies'], 
                                                       row['api_distance'],
                                                       row['distance_ratio'], 
                                                       row['api_duration'],
                                                       row['api_speed']))



    # print("Directions API response")
    # print("=======================")
    # print("Query_url: {0}".format(query_string))
    # print("Distance /miles: {0:0.2f}".format(distance/METRES_PER_MILE) )
    # print("Duration /minutes: {0:0.2f}".format(duration/60.0) )
    # print("Start address: {0}".format(start_address) )
    # print("End address: {0}".format(end_address) )

def get_station_data(start_station, finish_station):
    journey = {}
    journey['start'] = start_station
    journey['finish'] = finish_station

    olat, olng = get_station_lat_lng(start_station)
    flat, flng = get_station_lat_lng(finish_station)
    # Calculate the crow-flies distance for each station
    journey['crowflies'] = 60.0 * math.sqrt((flat - olat)**2 + (flng - olng)**2 )

    # For each station at less than the threshold distance *2, call Google Directions
    journey['api_distance'] = None
    journey['api_duration'] = None
    journey['api_speed'] = None
    journey['distance_ratio'] = None
    if (journey['crowflies'] < 2.0 * walk_threshold):
        directions = call_directions_api(olat, olng, flat, flng)
        if directions['status'] == 'OK':
            journey['api_distance'] = directions['distance'] / METRES_PER_MILE
            journey['api_duration'] = directions['duration'] / 60.0
            if journey['crowflies'] > 0.01:
                journey['distance_ratio'] = journey['api_distance'] / journey['crowflies']
            else:
                journey['distance_ratio'] = 1.0

            if journey['api_duration'] > 0.01:
                journey['api_speed'] = 60.0 * journey['api_distance'] / journey['api_duration']
            else:
                journey['api_speed'] = 2.825
         
    return journey

def call_mapbox_api():
    api_key = os.environ['MAPBOX_API_KEY']
    query_template = "https://api.mapbox.com/distances/v1/mapbox/{md}?access_token={api}"
    query_string = query_template.format(md="walking", api=api_key)

    data = {
        "coordinates": [
        [ 13.41894, 52.50055 ],
        [ 14.10293, 52.50055 ],
        [ 13.50116, 53.10293 ]
        ]
    }

    headers = {'Content-type': 'application/json'}
    r = requests.post(query_string, data=json.dumps(data), headers=headers)

    print(r.content)

def call_directions_api(olat, olng, flat, flng):
    # Sample API call
    # mode options= walking, bicyling, 
    # https://maps.googleapis.com/maps/api/directions/json?origin=Brooklyn&destination=Queens&mode=transit&key=API_KEY
    clean_directions = {}
    api_key = os.environ['DIRECTIONS_API_KEY']
    query_template = "https://maps.googleapis.com/maps/api/directions/json?origin={org}&destination={dest}&mode={md}&key={api}"

    org = "{},{}".format(olat, olng) 
    dest = "{},{}".format(flat, flng)

    query_string = query_template.format(org=org, dest=dest, md="walking", api=api_key)

    r = requests.get(query_string)
    directions = r.json()

    # Status not OK when trying to travel by foot to Heathrow
    if directions['status'] != 'OK':
        print("Directions status not OK for {} to {}".format(org, dest))
        print(directions)
        clean_directions['distance'] = None
        clean_directions['duration'] = None 
        clean_directions['start_address'] = None
        clean_directions['end_address'] = None
        clean_directions['status'] = directions['status']
        return clean_directions
    clean_directions['distance'] = float(directions['routes'][0]['legs'][0]['distance']['value']) #In metres
    clean_directions['duration'] = directions['routes'][0]['legs'][0]['duration']['value'] #In seconds
    clean_directions['start_address'] = directions['routes'][0]['legs'][0]['start_address']
    clean_directions['end_address'] = directions['routes'][0]['legs'][0]['end_address']
    clean_directions['status'] = directions['status']
    return clean_directions

def get_station_lat_lng(station):
    global stations
    for row in stations:
        if row['station'] == station:
            lat = float(row['lat'])
            lng = float(row['lng'])
            break
    return lat, lng

def read_stations_file(filename):
#    rowid,Station,LineS,LocalAuthority,Opened,ZoneS,Usage,nlines,first_line,latitude,longitude,x,y,ground level,depth
    TEMPLATE_ROW = OrderedDict([
        ('id', None),
        ('station', None),
        ('lat', None),
        ('lng', None),
        ('state', None),
        ('distance', None)
    ])
    stations = []

    with open(filename, 'rb') as f:
        reader = csv.reader(f)
        _ = reader.next() #we're ignoring headers
        for row in reader:
            if not row:
                break
            station = TEMPLATE_ROW.copy()
            station['id'] = row[0]
            station['station'] = row[1]
            station['lat'] = row[9]
            station['lng'] = row[10]
            stations.append(station)

    return stations


if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    #call_mapbox_api()
    main()
    