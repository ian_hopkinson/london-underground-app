var map = null
var stations = null
var labels = null
var station_array = new Array()
var label_array = new Array()
var speed = 20.00

var redIcon = L.icon({iconUrl: 'static/roundel-red-sml.png'});
var orangeIcon = L.icon({iconUrl: 'static/roundel-orange-sml.png'});
var blueIcon = L.icon({iconUrl: 'static/roundel-textless-sml-grey.png'});

var googleMapsStub = 'http://maps.google.com/maps?'

function getThresholdInMiles(){
  var threshold = document.getElementById("walk-limit").value
  if ($('.minutes').hasClass("active")) {
    threshold = threshold/speed
  }
  return threshold
}

function selectStation(e){
  var originel = document.getElementById("origin");
  originText = originel.options[originel.selectedIndex].text;
  threshold = getThresholdInMiles()

  if (originText == "Select a station"){
    $('#no-station-error').show()
    return false
  }
  if (isNaN(threshold)){
    $('#invalid-threshold-error').show();
    return false
  }

  $('#no-station-error').hide()
  $('#invalid-threshold-error').hide();
  document.getElementById("origin-legend").innerHTML = originText
  stations = addStationMarkers("/stations/" + originText + "/" + threshold)
}

/*function milesMinutesToggle(e){
  console.log("Hit miles/minutes toggle")
}*/

function populateTable(tableContents) {
// http://stackoverflow.com/questions/14643617/create-table-using-javascript
// Get tbody element
  var table = document.getElementById("result-table-body");
// Delete current contents of body
  table.innerHTML = ""
// Return if we have no distance data
  
  if (tableContents[0].state == null){
    var row = table.insertRow(-1)
    var cell1 = row.insertCell(0)
    cell1.innerHTML = 'No station selected'
    cell1.colSpan = 4  
    return
  }
// Sort tableContents on distance
tableContents.sort(function(a, b){
  return a.distance-b.distance;
});

// Add rows inside threshold
  var threshold = getThresholdInMiles()
  for(var i = 0; i < tableContents.length; i++){
      if (tableContents[i].distance > threshold){
        return
      } 
      // Make a googe directions URL http://maps.google.com/maps?saddr=51.532968,+-0.105567&amp;daddr=51.520196,+-0.104829&amp;dirflg=w
      var query = {
                    saddr:tableContents[0].lat + "," + tableContents[0].lng,
                    daddr:tableContents[i].lat + "," + tableContents[i].lng,
                    dirflg: "w"
                  };
      var querystr = $.param( query );
      var row = table.insertRow(-1)
      var cell1 = row.insertCell(0)
      var cell2 = row.insertCell(1)
      var cell3 = row.insertCell(2)
      var cell4 = row.insertCell(3)
      cell1.innerHTML = tableContents[i].station
      cell2.innerHTML = tableContents[i].distance.toFixed(2)
      cell3.innerHTML = (tableContents[i].distance * speed).toFixed(0)
      cell4.innerHTML = '<a href=\"' + googleMapsStub + querystr + '\" target=\"_blank"\">Show Me</a>'
      //var tr = tbl.insertRow();
      //var td = tr.insertCell();
       //       td.appendChild(document.createTextNode('Cell'));
      }
}

function findStationFromClick(e){
  var olat = e.latlng.lat
  var olng = e.latlng.lng
  var nearestStation = null

  findStationFromLatLng(olat, olng, e)
}

function findStationFromLatLng(olat, olng, e){
  $.getJSON("/stations", function(station_data){
    var mindist = 10.0 
    for (var i = 0; i < station_data.length; i++){
      var dist = Math.sqrt(Math.pow(olat-station_data[i].lat,2) + Math.pow(olng-station_data[i].lng,2))
      if (dist < mindist) {
        mindist = dist
        nearestStation = station_data[i].station
      }
    }
    var originel = document.getElementById("origin");
    originel.value = nearestStation
    selectStation(e)
  }); 
}

function pressRefresh(e){
  selectStation(e)
}

function getLocation(e){
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(function(position) {
    console.log(position.coords.latitude + "," + position.coords.longitude);
    findStationFromLatLng(position.coords.latitude, position.coords.longitude, e)
    });
  } else {
  console.log("No geolocation available")
  $("#location-btn").attr("style", "display:none");
  }
}

function addStationMarkers(origin){
    if (stations !== null) {
      map.removeLayer(stations);
      map.removeLayer(labels);
    }

    if (labels !== null) {
      map.removeLayer(labels);
    }

    $.getJSON( origin , function( station_data ) {
    //station_array.length = 0
    station_array = new Array()
    label_array = new Array()
    var marker = null
    for (var i = 0; i < station_data.length; i++) {
      if (station_data[i].state !== null) {
        colour = station_data[i].state}
      else {
        colour = "blue"}

      if (station_data[i].state == "red"){
        map.setView(new L.LatLng(station_data[i].lat, station_data[i].lng), 12);
      }

      switch(station_data[i].state) {
      case "red":
        icon = redIcon
        break;
      case "orange":
        icon = orangeIcon
        break;
      default:
        icon = blueIcon
      }

      marker = L.marker([station_data[i].lat,station_data[i].lng], {icon:icon})
      
      if (station_data[i].distance !== null) {
        label = station_data[i].station + "<br>" + station_data[i].distance.toFixed(2) + " miles"}
      else {
        label = station_data[i].station}
      marker.bindPopup(label)
      // This shows the popup on mouseover rather than click

      marker.on('click', function (e) {
        findStationFromClick(e);
      });
      marker.on('mouseover', function (e) {
            this.openPopup();
      });
      marker.on('mouseout', function (e) {
            this.closePopup();
      });
      station_array.push(marker)
      
      // Add label array
      var myIcon = L.divIcon({html: station_data[i].station, 
                              className: 'my-div-icon'});

      divLabel = L.marker([station_data[i].lat,station_data[i].lng],
                 {icon: myIcon})
      label_array.push(divLabel)
        //.addTo(map);
 
    }

    
    labels = L.layerGroup(label_array);

    if (document.getElementById('showLabels-cb').checked){
      labels.addTo(map)
    }

    stations = L.layerGroup(station_array);
    stations.addTo(map);

    populateTable(station_data);
  });
}

function showLabels(e){
 if (e.checked){
      labels.addTo(map)
    } else {
      map.removeLayer(labels)
      //labels.addTo(map)
    }  
}

// This is the initialisation code
$(document).ready(function(){
  // Populate dropdown
   options = '<option>Select a station</option>';
   $.getJSON("/stations", function(stations){
    $.each(stations, function(index, value){options += '<option value="'+ value.station +'">' + value.station + '</option>';})
    $("#origin").html(options);
  });

  //Define bounding box for map
  var southWest = L.latLng(51.38222, -0.6109864),
      northEast = L.latLng(51.70522, 0.25177),
      bounds = L.latLngBounds(southWest, northEast);

  // Create map
  map = L.map('map', {
    maxBounds: bounds
  }) 
  // Create tiles
  L.tileLayer('http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    center: [51.55372,-0.4027927],
    bounds: bounds,
    //zoom: 5,
    minZoom: 10 
  }).addTo(map);


  //Set bounds to fit
  map.fitBounds(bounds);
  
  stations = addStationMarkers("/stations")

  // Register events
  map.on('click', function(e) {
    findStationFromClick(e);   
  });

  $(function(){
        $('#origin').change(selectStation);
  });

  var walk_limit = document.getElementById('walk-limit');
  walk_limit.addEventListener('keypress', function(event) {
    if (event.keyCode == 13) {
                event.preventDefault();
                document.getElementById('refresh-btn').click();
              }
  });

  $('.miles-minutes-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').size()>0) {
      $(this).find('.btn').toggleClass('btn-primary');
    }

    if ($('.minutes').hasClass("active")){
      document.getElementById("walk-limit").value = document.getElementById("walk-limit").value * speed
    } else {
      document.getElementById("walk-limit").value = document.getElementById("walk-limit").value / speed      
    }
     
  });

});





