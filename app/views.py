#!/usr/bin/env python
# encoding: utf-8

from flask import render_template, url_for, request

from app import app

import csv
import json
import math

from collections import OrderedDict

stations = []
#walking_threshold = 1.5

@app.route('/', methods=['GET', 'POST'])
def index():
    content = render_template("index.html")
    return content

@app.route('/stations', methods=['GET', 'POST'])
def getStations():
    global stations
    return json.dumps(stations)

@app.route('/about', methods=['GET', 'POST'])
def about():
    content = render_template("about.html")
    return content

@app.route('/stations/<origin>/<threshold>', methods=['GET', 'POST'])
def getWalking(origin, threshold):
    global stations
    #walking_threshold = request.form['walk-limit']
    #print walking_threshold
    try:
        walking_threshold = float(threshold)
    except ValueError:
        walking_threshold = 1.5

    for row in stations:
        if row['station'] == origin:
            origin_lat = float(row['lat'])
            origin_lng = float(row['lng'])
            break

    walking = []
    for row in stations:
        row['distance'] = 60.0 * math.sqrt((float(row['lat']) - origin_lat)**2 +(float(row['lng']) - origin_lng)**2 )
        if row['station'] == origin:
            row['state'] = 'red'
        elif row['distance'] < walking_threshold:
            row['state'] = 'orange'
        else:
            row['state'] = 'blue'
        walking.append(row)

    return json.dumps(walking)

def read_stations_file(filename):
#    rowid,Station,LineS,LocalAuthority,Opened,ZoneS,Usage,nlines,first_line,latitude,longitude,x,y,ground level,depth
    TEMPLATE_ROW = OrderedDict([
        ('id', None),
        ('station', None),
        ('lat', None),
        ('lng', None),
        ('state', None),
        ('distance', None)
    ])
    stations = []

    with open(filename, 'rb') as f:
        reader = csv.reader(f)
        _ = reader.next() #we're ignoring headers
        for row in reader:
            if not row:
                break
            station = TEMPLATE_ROW.copy()
            station['id'] = row[0]
            station['station'] = row[1]
            station['lat'] = row[9]
            station['lng'] = row[10]
            stations.append(station)

    return stations

stations = read_stations_file("app/static/station_data.csv") 