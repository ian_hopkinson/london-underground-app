run:    build
	@docker run \
	    -p 8000:8000 \
	    --read-only \
	    --rm \
	    --volume /tmp \
	    ianhopkinson/london_underground_app

build:
	@docker build -t ianhopkinson/london_underground_app .

.PHONY: run build