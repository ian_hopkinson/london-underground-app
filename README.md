# London Underground App
 Ian Hopkinson 2015-08-08

 A web app which replicates the functionality of the [Should I walk it?](https://blog.scraperwiki.com/2014/05/the-london-underground-should-i-walk-it/) and the 
 [London Underground visualisation](https://blog.scraperwiki.com/2014/04/visualising-the-london-underground-with-tableau-2/)

Read more on the [launch blog](http://www.ianhopkinson.org.uk/2015/08/the-london-underground-can-i-walk-it/).

## Developer instructions

To run locally do:

`python local_run.py`

To use Docker make sure your Docker daemon is running then do:

1. `make run`

App accessible via http://localhost:8000 (on ubuntu)

Using `boot2docker` on Windows or OS X:

1. Do `boot2docker up`
2. `make run` 
2. `boot2docker ip` to find the IP address
3. Then visit the indicated IP address on port 8000

To create a Docker image (this only works inside my Ubuntu VM):

`docker login`

and:

`make build`

Do:
`docker push ianhopkinson/london_underground_app`

## Deploy to Digital Ocean

Create a Docker Droplet, register your ssh keys then use ssh to access:

`ssh root@46.101.8.17`

`docker ps`

`docker pull ianhopkinson/london_underground_app`

`docker stop [image name]`

`docker run -d -p 80:8000 ianhopkinson/london_underground_app`

Then navigate to:
`http://46.101.8.17/`

The instructions I used were here:

http://stackoverflow.com/questions/30692064/how-do-i-deploy-this-docker-app-on-digital-ocean

It took me some fiddling to get this working, as usual my problem was with port mapping.

Instructions for domain name settings:

https://www.digitalocean.com/community/tutorials/how-to-set-up-a-host-name-with-digitalocean


## Todo

21. Add About page to site - needs a navbar, can't find the "map" element
22. Consider making a "technical details page" 
20. Add a cycling option (presumably just need to vary my speed parameter @GoodCoffeeCode Walking ~5kph, cycling ~15kph)
21. Query Logging - how does this work with Docker? Should I use Google Analytics 
18. Cache stations data
20. Do threshold in frontend not backend
21. Add lines joining stations
22. Calculate walking distances by Google Directions API (Not easy naively, 270 stations only 2500 API calls allowed per 24 hours 270*270/2 = 36450)

## Known issues:

* We can't see the float overs on touch device, on desktop they flicker
* On the live site refreshing the page resets the select and legend but not the map itself

## Assets

David Hughes form gist:

https://gist.github.com/skillsaware/48f7b800ef475396b991

This Flickr account has the underground map through the ages:

https://www.flickr.com/photos/33957578@N07/

leaflet.js tile providers:

http://leaflet-extras.github.io/leaflet-providers/preview/index.html

A tutorial on doing forms in Bootstrap

http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-forms.php
